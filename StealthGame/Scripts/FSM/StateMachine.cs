using Godot;
using System.Collections.Generic;

namespace StealthGame.Scripts.FSM
{
    public class StateMachine : Node
    {
        State m_state = null;

        public State State
        {
            get { return m_state; }
            set { SetState(value); }
        }

        public State PreviousState = null;

        public Dictionary<string, State> States = new Dictionary<string, State>();
        

        public override void _PhysicsProcess(float delta)
        {
            if (m_state != null)
            {
                StateLogic(delta);
                State transition = GetTransition(delta);

                if (transition != null)
                {
                    SetState(transition);
                }
            }
        }

        public virtual void StateLogic(float delta)
        {
        }

        public virtual State GetTransition(float delta)
        {
            return null;
        }

        public virtual void EnterState(State newState, State oldState)
        {
        }

        public virtual void ExitState(State oldState, State newState)
        {
        }

        public void SetState(State newState)
        {
            PreviousState = m_state;
            m_state = newState;

            if (PreviousState != null)
            {
                ExitState(PreviousState, newState);
            }

            if (newState != null)
            {
                EnterState(newState, PreviousState);
            }
        }

        public void AddState(string stateName)
        {
            State stateToAdd = new State();
            States.Add(stateName, stateToAdd);
            stateToAdd.StateName = stateName;
        }
    }
}