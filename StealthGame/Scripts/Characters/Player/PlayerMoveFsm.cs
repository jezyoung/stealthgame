using Godot;
using System.Collections.Generic;
using StealthGame.Scripts.FSM;

namespace StealthGame.Scripts.Characters.Player
{
    public class PlayerMoveFsm : StateMachine
    {
        private string idleState = "IdleState";
        private string sneakState = "SneakState";
        private string runState = "RunState";
        private string climbState = "ClimbState";

        private PlayerMover m_playerMover;

        public override void _Ready()
        {
            base._Ready();
            AddState(idleState);
            AddState(sneakState);
            AddState(runState);
            AddState(climbState);

            CallDeferred(nameof(SetState), States[idleState]);
        }

        void _on_PlayerMover_ready()
        {
            m_playerMover = GetParent() as PlayerMover;
        }
        
        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
        }

        public override void StateLogic(float delta)
        {
            base.StateLogic(delta);
        }

        public override State GetTransition(float delta)
        {
            if (State == States[idleState])
            {
                if (m_playerMover.Velocity != Vector2.Zero)
                {
                    if (m_playerMover.Sneaking)
                    {
                        return States[sneakState];
                    }

                    return States[runState];
                }
            }

            else if (State == States[sneakState])
            {
                if (!m_playerMover.Sneaking)
                {
                    return States[runState];
                }
                if (m_playerMover.Velocity == Vector2.Zero)
                {
                    return States[idleState];
                }
            }

            else if (State == States[runState])
            {
                if (m_playerMover.Sneaking)
                {
                    return States[sneakState];
                }
                
                if (m_playerMover.Velocity == Vector2.Zero)
                {
                    return States[idleState];
                }
            }

            else if (State == States[climbState])
            {
            }

            return null;
        }

        public override void EnterState(State newState, State oldState)
        {
            if (newState == States[idleState])
            {
                GD.Print("Idle");
            }
            
            else if (newState == States[sneakState])
            {
                GD.Print("Sneaking");
            }
            
            else if (newState == States[runState])
            {
                GD.Print("Running");
            }
        }

        public override void ExitState(State oldState, State newState)
        {
            base.ExitState(oldState, newState);
        }
    }
}