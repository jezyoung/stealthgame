using Godot;
using System;

namespace StealthGame.Scripts.Characters.Player
{
    public enum PLAYERSTATES{Normal, Stealth, Hiding, Interacting}
    
    public class PlayerController: KinematicBody2D
    {
        public PLAYERSTATES PlayerState;
        
        public PlayerInput PlayerInput;
        public PlayerMover PlayerMover;

        public override void _Ready()
        {
            PlayerInput = GetNode<Node2D>("PlayerInput") as PlayerInput;
            PlayerMover = GetNode<Node2D>("PlayerMover") as PlayerMover;
            
        }

        public override void _PhysicsProcess(float delta)
        {
            Movement(delta);
        }

        private void Movement(float delta)
        {
            Vector2 velocity = PlayerMover.Move(PlayerInput.HorizontalInput, PlayerInput.VerticalInput, delta, PlayerState);
            velocity = MoveAndSlide(velocity);
        }
    }
}