using Godot;
using System;

namespace StealthGame.Scripts.Characters.Player
{
    public class PlayerInput: Node2D
    {
        public float HorizontalInput, VerticalInput;
        public bool RunInput = false;
        
        public override void _PhysicsProcess(float delta)
        {
            HorizontalInput = Input.GetActionStrength("ui_right") - Input.GetActionStrength("ui_left");
            VerticalInput = Input.GetActionStrength("ui_down") - Input.GetActionStrength("ui_up");
            
            RunInput = Input.IsActionPressed("ui_run");
        }
    }
}