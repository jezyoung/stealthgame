using Godot;
using System;


namespace StealthGame.Scripts.Characters.Player
{
    public class PlayerMover: Node2D
    {
        [Export()] private float acceleration = 512;
        [Export()] private float maxSneakSpeed = 64;
        [Export()] private float maxRunSpeed = 192;
        [Export()] private float friction = 0.25f;

        private bool m_sneaking = true;
        public bool Sneaking => m_sneaking;

        private Vector2 m_velocity = Vector2.Zero;
        public Vector2 Velocity => m_velocity;
        
        public Vector2 Move(float horizontal, float vertical, float delta, PLAYERSTATES state)
        {
            if (horizontal != 0)
            {
                float maxSpeed = 64;

                if (state == PLAYERSTATES.Stealth)
                {
                    maxSpeed = maxSneakSpeed;
                    m_sneaking = true;
                }
                else if (state == PLAYERSTATES.Normal)
                {
                    maxSpeed = maxRunSpeed;
                    m_sneaking = false;
                }
                
                m_velocity.x += horizontal * acceleration * delta;
                m_velocity.x = Mathf.Clamp(m_velocity.x, -maxSpeed, maxSpeed);
            }
            else
            {
                m_velocity.x = Mathf.Lerp(m_velocity.x, 0, friction);
            }

            return m_velocity;
        }
    }
}