using Godot;
using System.Collections.Generic;
using StealthGame.Scripts.FSM;

namespace StealthGame.Scripts.Characters.Player
{
    public class PlayerFsm: StateMachine
    {
        private string normalState = "NormalState";
        private string stealthState = "StealthState";
        private string hidingState = "HidingState";
        private string interactingState = "InteractingState";

        private PlayerController m_playerController;
        
        public override void _Ready()
        {
            base._Ready();
            AddState(normalState);
            AddState(stealthState);
            AddState(hidingState);
            AddState(interactingState);
            
            CallDeferred("SetState", States[normalState]);
        }

        void _on_Player_ready()
        {
            m_playerController = GetParent() as PlayerController;
        }

        public override void _PhysicsProcess(float delta)
        {
            base._PhysicsProcess(delta);
        }

        public override void StateLogic(float delta)
        {
            base.StateLogic(delta);
        }

        public override State GetTransition(float delta)
        {
            if (State == States[normalState])
            {
                if (!m_playerController.PlayerInput.RunInput)
                {
                    return States[stealthState];
                }
            }
            
            else if (State == States[stealthState])
            {
                if (m_playerController.PlayerInput.RunInput)
                {
                    return States[normalState];
                }
            }
            
            else if (State == States[hidingState])
            {
                
            }
            
            else if (State == States[interactingState])
            {
                
            }

            return null;
        }

        public override void EnterState(State newState, State oldState)
        {
            if (newState == States[normalState])
            {
                m_playerController.PlayerState = PLAYERSTATES.Normal;
                GD.Print("Normal");
            }
            
            else if (newState == States[stealthState])
            {
                m_playerController.PlayerState = PLAYERSTATES.Stealth;
                GD.Print("Stealth");
            } 
            
            else if (newState == States[hidingState])
            {
                m_playerController.PlayerState = PLAYERSTATES.Hiding;
            } 
            
            else if (newState == States[interactingState])
            {
                m_playerController.PlayerState = PLAYERSTATES.Interacting;
            } 
        }

        public override void ExitState(State oldState, State newState)
        {
            base.ExitState(oldState, newState);
        }
        
    }
}